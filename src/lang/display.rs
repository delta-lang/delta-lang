use lang::*;

pub trait AsStr {
	fn as_str(&self) -> Str;
}