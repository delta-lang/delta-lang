use lang::*;
use lang::syntax::token::token_types::*;
use error::*;

#[derive(Clone, Debug)]
pub struct ArrayDef {
	name: Name,
	values: Vec<Handle>
}

impl ArrayDef {
	fn new(name: Name, values: Vec<Handle>) -> Self {
		ArrayDef {
			name: name,
			values: values
		}
	}

	#[inline]
	pub fn get_name(&self) -> Name {
		self.name.clone()
	}
}

impl Evaluate for ArrayDef {
	fn eval_with_frame(&self, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
		let values: Result<_, ScriptError> = self.values.iter().map(|value| {
			let value = try!(EvalContext::eval_expr_with_frame(value, frame));
			Ok(Value::from(value))
		}).collect();
		let array = Array::new(try!(values));
		Ok(Value::Arr(array).into())
	}
}

impl MatchConstruct for ArrayDef {
    fn match_construct(tree: &mut ExprTreeIterator) -> Option<Vec<Handle>> {
		let mut iter = tree.iter();
		let ident = match Keyword::find_arr_token(&mut iter)
			.and_then(|_| Identity::find_identity_token(&mut iter)) {
			Some(ident) => ident,
			None => return None
		};
		if let None = OperatorToken::find_assign_token(&mut iter)
			.and_then(|_| Punctuator::find_open_square_brace_token(&mut iter)) {
			return None;
		};
		let mut elements = Vec::new();
		while let Some(element) = Relation::match_expr(&mut iter) {
			elements.push(element);
			if let None = Punctuator::find_comma_token(&mut iter) {
				break;
			}
		};
		if let None = Punctuator::find_close_square_brace_token(&mut iter) {
			return None;
		};
		let arr = ArrayDef::new(ident.clone().into(), elements);
		let arr_handle = Handle::new(Expr::from(arr));
		let mut ret = Vec::new();
		let name = Handle::new(<Expr as From<Name>>::from(ident.into()));
		let name_rel = Handle::new(Expr::from(Relation::Nullary(name)));
		let decl = Handle::new(Expr::from(Relation::Unary(Operator::Let, arr_handle.clone())));
		let assign = Handle::new(Expr::from(Relation::Binary(Operator::Assign, name_rel, arr_handle)));
		ret.push(decl);
		ret.push(assign);
		tree.advance_to(iter);
		Some(ret)
	}
}
