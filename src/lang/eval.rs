use lang::*;
use error::*;

pub struct EvalContext {
	stack: Vec<StackFrame>
}

impl EvalContext {
	pub fn new() -> Self {
		let stack = vec![StackFrame::new()];
		EvalContext { stack: stack }
	}

	pub fn load_package(&mut self, pkg: Package) {
		let ref mut global_scope = self.stack[0];
		pkg.import_into_scope(global_scope);
	}

	pub fn eval_expr(&mut self, expr: &Handle) -> Result<LValue, ScriptError> {
		match self.stack.last_mut() {
			Some(ref mut frame) => Self::eval_expr_with_frame(expr, frame),
			None => Err(ScriptError::from(InternalError::CorruptedStack))
		}
	}

	pub fn eval_expr_with_frame(expr: &Handle, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
		match expr.borrow().kind {
			ExprKind::Rel(ref rel) => Self::eval_rel_with_frame(rel, frame),
			ExprKind::Closure(ref closure) => closure.eval_with_frame(frame),
			ExprKind::Obj(ref obj) => obj.eval_with_frame(frame),
			ExprKind::Arr(ref array) => array.eval_with_frame(frame),
			ExprKind::Name(ref name) => match frame.get_mut(name) {
				Some(stack_value) => {
					let value: Value = stack_value.clone().into();
					Ok(value.into())
				},
				_ => return Err(ScriptError::from(ReferenceError::NotDeclared(name.clone().into()))),	
			},
			ref t @ _ => {
				let msg = format!("Case `{:?}` is not yet implemented.", t);
				Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
			},
		}
	}

	pub fn eval_rel_with_frame(rel: &Relation, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
		match rel.eval_with_frame(frame) {
			Ok(lval) => {
				if let Some(name) = lval.name.clone() {
					frame.set(name, lval.clone().into());
				}
				Ok(lval)
			},
			Err(e) => Err(e)
		}
	}
}

pub trait Evaluate {
	fn eval_with_frame(&self, &mut StackFrame) -> Result<LValue, ScriptError>;
}

pub trait CallAsFunction {
	fn call(&self, Arguments, &mut StackFrame) -> Result<Value, ScriptError>;
}
