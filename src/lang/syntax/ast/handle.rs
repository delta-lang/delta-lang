use std::ops::Deref;
use std::rc::{Rc, Weak};
use std::cell::RefCell;

use lang::Expr;

/**

	TODO:
	- Rename `Handle` to `AstNode`

 */


#[derive(Clone, Debug)]
pub struct Handle(Rc<RefCell<Expr>>);

impl Handle {
	pub fn new(expr: Expr) -> Self {
		Handle(Rc::new(RefCell::new(expr)))
	}
}

impl Deref for Handle {
	type Target = Rc<RefCell<Expr>>;
    fn deref(&self) -> &Self::Target {
    	&self.0
    }
}

#[derive(Clone)]
pub struct WeakHandle(Weak<RefCell<Expr>>);

impl<'a> From<&'a Handle> for WeakHandle {
	fn from(handle: &'a Handle) -> WeakHandle {
		WeakHandle(Rc::downgrade(handle))
	}
}