use std::collections::HashMap;
use lang::*;

#[derive(Clone, Debug)]
pub struct Object {
	name: Name,
	values: HashMap<Name, Value>
}

impl Object {
	#[inline]
	pub fn new(name: Name, values: HashMap<Name, Value>) -> Self {
		Object {
			name: name,
			values: values
		}
	}

	#[inline]
	pub fn get_name(&self) -> Name {
		self.name.clone()
	}

	#[inline]
    pub fn get_mut(&mut self, name: &Name) -> Option<&mut Value> {
    	self.values.get_mut(name)
    }

    #[inline]
    pub fn get(&self, name: &Name) -> Option<&Value> {
        self.values.get(name)
    }
}

impl Typename for Object {
	fn typename(&self) -> Name {
		Name::new("Object".to_owned())
	}
}

impl AsStr for Object {
	fn as_str(&self) -> Str {
		let name: String = self.name.clone().into();
		let n = format!("{} {{ [ props ] }}", name);
		Str::new(n)
	}
}