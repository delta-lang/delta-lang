use regex::Regex;
use lang::*;

static OPERATOR_ADD_TOKEN_REGEX: &'static str = "^([+])";
static OPERATOR_SUB_TOKEN_REGEX: &'static str = "^([-])";
static OPERATOR_ASSIGN_TOKEN_REGEX: &'static str = "^([=])";

#[derive(Debug, Clone, PartialEq)]
pub enum OperatorToken {
	Add,
	Subtract,
	Assign,
}

impl OperatorToken {
	pub fn find_assign_token(tree: &mut ExprTreeIterator) -> Option<OperatorToken> {
        match tree.next() {
            Some( &Token { kind: TokenKind::Operator(ref kind @ _), .. }) 
            	if *kind == OperatorToken::Assign => Some(kind.clone()),
            _ => None,
        }
	}
}

impl MatchToken for OperatorToken {
	fn match_token(text: &str) -> Option<Token> {
		match_add(text)
			.or_else(|| match_sub(text))
			.or_else(|| match_equals(text))
	}
}


fn match_add(text: &str) -> Option<Token> {
	let add_rx = Regex::new(OPERATOR_ADD_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Operator::Add: {:?}. Error {:?}", OPERATOR_ADD_TOKEN_REGEX, e);
	});

	match add_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Operator(OperatorToken::Add)
			))
		},
		None => None
	}
}

fn match_equals(text: &str) -> Option<Token> {
	let eq_rx = Regex::new(OPERATOR_ASSIGN_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Operator::Assign `{:?}`. Error {:?}", OPERATOR_ASSIGN_TOKEN_REGEX, e);
	});

	match eq_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new( 
				TokenMatchPos(begin, end), 
				TokenKind::Operator(OperatorToken::Assign)
			))
		},
		None => None
	}
}

fn match_sub(text: &str) -> Option<Token> {
	let sub_rx = Regex::new(OPERATOR_SUB_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Operator::Subtract `{:?}`. Error {:?}", OPERATOR_SUB_TOKEN_REGEX, e);
	});

	match sub_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Operator(OperatorToken::Subtract)
			))
		},
		None => None
	}
}