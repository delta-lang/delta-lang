#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Name(String);

impl Name {
	pub fn new(name: String) -> Self {
		Name(name)
	}
}

impl Into<String> for Name {
	fn into(self) -> String {
		self.0
	}
}