use lang::*;
use lang::syntax::token::token_types::*;
use error::*;

#[derive(Debug, Clone)]
pub enum Operator {
	Add,
	Subtract,
	Assign,
	Let, // TODO rename to Declare - Jon 2/02/16
	Call(CallOperator),
	Dot(DotOperator),
	Index(IndexOperator)
}

impl From<OperatorToken> for Operator {
	fn from(token: OperatorToken) -> Self {
		match token {
			OperatorToken::Add => Operator::Add,
			OperatorToken::Subtract => Operator::Subtract,
			OperatorToken::Assign => Operator::Assign
		}
	}
}

impl From<CallOperator> for Operator {
	fn from(call: CallOperator) -> Self {
		Operator::Call(call)
	}
}

impl Operator {
	pub fn match_operator(tree: &mut ExprTreeIterator) -> Option<Operator> {
		Operator::new_from_tokens(tree)
			.or_else(|| CallOperator::new(tree).and_then(|token| Some(Operator::from(token))))
			.or_else(|| DotOperator::match_operator(tree))
			.or_else(|| IndexOperator::match_operator(tree))
	}

	fn new_from_tokens(tree: &mut ExprTreeIterator) -> Option<Operator> {
		let mut iter = tree.iter();
		if let Some(token) = iter.next() {
			return match token.kind {
				TokenKind::Operator(ref op_token) => {
					tree.advance_to(iter);
					Some(Operator::from(op_token.clone()))
				},
				_ => None
			}
		}
		None
	}
}

impl UnaryFunction for Operator {
    fn call(&self, a: &Handle, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
    	match *self {
    		Operator::Let => match a.borrow().kind {
				ExprKind::Name(ref name) => Ok(LValue::new(Some(name.clone()), Value::Undefined)),
				ExprKind::FnDef(ref func) => Ok(LValue::new(Some(func.get_name()), Value::Undefined)),
				ExprKind::Obj(ref obj) => Ok(LValue::new(Some(obj.get_name()), Value::Undefined)),
				ExprKind::Arr(ref arr) => Ok(LValue::new(Some(arr.get_name()), Value::Undefined)),
				ref e @ _ => {
	    			let msg = format!("Evaluation of `{:?}` is not yet supported.", e);
	    			Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
	    		},	
    		},
    		Operator::Call(ref call) => call.call(a, frame),
    		Operator::Dot(ref dot) => dot.call(a, frame),
    		Operator::Index(ref idx) => idx.call(a, frame),
    		ref e @ _ => {
    			let msg = format!("Operator `{:?}` is not yet supported.", e);
    			Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
    		},
    	}
    }
}

impl BinaryFunction for Operator {
	fn call(&self, a: &Handle, b: &Handle, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
		let lvalue = try!(EvalContext::eval_expr_with_frame(a, frame));
		let rvalue = try!(EvalContext::eval_expr_with_frame(b, frame));
		match *self {
			Operator::Assign => {
				match lvalue.name {
					Some(ref name) => {
						Ok(LValue::new(Some(name.clone()), rvalue.into()))
					},
					None => {
						let msg = format!("Cannot assign to an lvalue of {:?}", a);
						return Err(ScriptError::from(SyntaxError::Assignment(msg)));
					}
				}
			},
			Operator::Add => add_values(lvalue.value, rvalue.value),
			Operator::Subtract => subtract_values(lvalue.value, rvalue.value),
			_ => Err(ScriptError::from(SyntaxError::Unimplemented("TODO: Handle other binary operations".to_owned()))),
		}
	}
}

fn add_values(a: Value, b: Value) -> Result<LValue, ScriptError> {
	match (a, b) {
		(Value::Lit(c), Value::Lit(d)) => match (c, d) {
			(Lit::Num(ref e), Lit::Num(ref f)) => {
				let n = Lit::Num(Number::new(e.value() + f.value()));
				Ok(LValue::new(None, Value::Lit(n)))
			},
			(Lit::Str(ref e), Lit::Str(ref f)) => {
				let s = Lit::Str(Str::new(e.value() + &*f.value()));
				Ok(LValue::new(None, Value::Lit(s)))
			},
			(_, _) => {
				let msg = "Expected operands to be Numbers".to_owned();
				let err = TypeError::new("Number".to_owned(), "Other".to_owned(), msg);
				Err(ScriptError::from(SyntaxError::Type(err)))
			}
		},
		(_, _) => {
			let msg = "Expected operands to be Numbers".to_owned();
			let err = TypeError::new("Number".to_owned(), "Other".to_owned(), msg);
			Err(ScriptError::from(SyntaxError::Type(err)))
		}
	}
}

fn subtract_values(a: Value, b: Value) -> Result<LValue, ScriptError> {
	match (a, b) {
		(Value::Lit(c), Value::Lit(d)) => match (c, d) {
			(Lit::Num(ref e), Lit::Num(ref f)) => {
				let n = Lit::Num(Number::new(e.value() - f.value()));
				Ok(LValue::new(None, Value::Lit(n)))
			},
			(_, _) => {
				let msg = "Expected operands to be Numbers".to_owned();
				let err = TypeError::new("Number".to_owned(), "Other".to_owned(), msg);
				Err(ScriptError::from(SyntaxError::Type(err)))
			}
		},
		(_, _) => {
			let msg = "Expected operands to be Numbers".to_owned();
			let err = TypeError::new("Number".to_owned(), "Other".to_owned(), msg);
			Err(ScriptError::from(SyntaxError::Type(err)))
		}
	}
}