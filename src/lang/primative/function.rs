use std::boxed::Box;
use std::fmt;
use std::slice::Iter;

use lang::*;
use error::*;

#[derive(Debug, Clone)]
pub enum Function {
	Internal(Closure),
	External(ExternalFunction)
}

impl Typename for Function {
	fn typename(&self) -> Name {
		Name::new("Function".to_owned())
	}
}

impl AsStr for Function {
	fn as_str(&self) -> Str {
	    Str::new("Function".to_owned())
	}
}

impl CallAsFunction for Function {
	fn call(&self, args: Arguments, frame: &mut StackFrame) -> Result<Value, ScriptError> {
		match *self {
			Function::Internal(ref func) => func.call(args, frame),
			Function::External(ref func) => func.call(args, frame)
		}
	}
}

pub type InvocationCallback = Fn(Arguments, &mut StackFrame) -> Result<Value, ScriptError>;

#[derive(Clone)]
pub struct ExternalFunction {
	func: Box<&'static InvocationCallback>
}

impl ExternalFunction {
	pub fn new(func: Box<&'static InvocationCallback>) -> Self {
		ExternalFunction {
			func: func
		}
	}

	pub fn call(&self, args: Arguments, frame: &mut StackFrame) -> Result<Value, ScriptError> {
		(*self.func)(args, frame)
	}
}

impl fmt::Debug for ExternalFunction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Function [native code]")
    }
}

#[derive(Debug)]
pub struct Arguments {
	args: Vec<Value>
}

impl Arguments {
	pub fn new(args: Vec<Value>) -> Self {
		Arguments {
			args: args
		}
	}

    pub fn new_from_call_operator(call: CallOperator, frame: &mut StackFrame) -> Result<Arguments, ScriptError> {
    	let args = try!(call.args.iter().map(|handle| {
    		let expr = handle.borrow();
			match expr.kind {
		    	ExprKind::Lit(ref lit) => Ok(Value::from(lit.clone())),
		    	ExprKind::Name(ref name) => match frame.get(name) {
	    			Some(stack_val) => Ok(stack_val.clone().into()),
	    			_ => Err(ScriptError::from(ReferenceError::NotDeclared(name.clone().into())))
		    	},
		    	ref e @ _ => {
		    		let msg = format!("Call operator for `{:?}` is not yet supported.", e);
	    			Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
		    	}
	    	}
		}).collect());
        Ok(Arguments::new(args))
    }

    pub fn iter<'a>(&'a self) -> Iter<'a, Value> {
    	self.args.iter()
    }
}