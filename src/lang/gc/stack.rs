use lang::*;
use std::collections::HashMap;
use std::collections::hash_map::Iter;

pub struct StackFrame {
	pub locals: HashMap<Name, StackValue>
}

impl StackFrame {
    #[inline]
    pub fn new() -> Self {
    	StackFrame {
    		locals: HashMap::new()
    	}
    }

    #[inline]
    pub fn new_with_locals(locals: HashMap<Name, StackValue>) -> Self {
        StackFrame {
            locals: locals
        }
    }

    #[inline]
    pub fn set(&mut self, name: Name, value: Value) {
    	self.locals.insert(name, value.into());
    }

    #[inline]
    pub fn get_mut(&mut self, name: &Name) -> Option<&mut StackValue> {
    	self.locals.get_mut(name)
    }

    #[inline]
    pub fn get(&self, name: &Name) -> Option<&StackValue> {
        self.locals.get(name)
    }

    #[inline]
    pub fn iter(&self) -> Iter<Name, StackValue> {
        self.locals.iter()
    }
}