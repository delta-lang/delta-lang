use regex::Regex;
use lang::*;

static KEYWORD_LET_TOKEN_REGEX: &'static str = r"^let";
static KEYWORD_FN_TOKEN_REGEX: &'static str = r"^fn";
static KEYWORD_OBJ_TOKEN_REGEX: &'static str = r"^obj";
static KEYWORD_ARR_TOKEN_REGEX: &'static str = r"^arr";

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Keyword {
	Let,
	Fn,
	Obj,
	Arr
}

impl Keyword {
	pub fn find_obj_token(tree: &mut ExprTreeIterator) -> Option<Keyword> {
		match tree.next() {
			Some(&Token { kind: TokenKind::Keyword(ref kw @ _), .. }) if *kw == Keyword::Obj => Some(kw.clone()),
			_ => None,
		}
	}

	pub fn find_arr_token(tree: &mut ExprTreeIterator) -> Option<Keyword> {
		match tree.next() {
			Some(&Token { kind: TokenKind::Keyword(ref kw @ _), .. }) if *kw == Keyword::Arr => Some(kw.clone()),
			_ => None,
		}
	}
}

impl MatchToken for Keyword {
	fn match_token(text: &str) -> Option<Token> {
		match_let_token_in_text(text)
			.or_else(|| match_fn_token_in_text(text))
			.or_else(|| match_obj_token_in_text(text))
			.or_else(|| match_arr_token_in_text(text))
	}
}

fn match_let_token_in_text(text: &str) -> Option<Token> {
	let let_rx = Regex::new(KEYWORD_LET_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Keyword::Let `{:?}`. Error {:?}", KEYWORD_LET_TOKEN_REGEX, e);
	});

	match let_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Keyword(Keyword::Let)
			))
		},
		None => None
	}
}

fn match_fn_token_in_text(text: &str) -> Option<Token> {
	let let_rx = Regex::new(KEYWORD_FN_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Keyword::Fn `{:?}`. Error {:?}", KEYWORD_FN_TOKEN_REGEX, e);
	});

	match let_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Keyword(Keyword::Fn)
			))
		},
		None => None
	}
}

fn match_obj_token_in_text(text: &str) -> Option<Token> {
	let obj_rx = Regex::new(KEYWORD_OBJ_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Keyword::Obj `{:?}`. Error {:?}", KEYWORD_OBJ_TOKEN_REGEX, e);
	});

	match obj_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Keyword(Keyword::Obj)
			))
		},
		None => None
	}
}

fn match_arr_token_in_text(text: &str) -> Option<Token> {
	let arr_rx = Regex::new(KEYWORD_ARR_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Keyword::Arr `{:?}`. Error {:?}", KEYWORD_ARR_TOKEN_REGEX, e);
	});

	match arr_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Keyword(Keyword::Arr)
			))
		},
		None => None
	}
}