use regex::Regex;
use lang::{Token, TokenKind, TokenMatchPos, MatchToken};

static WHITESPACE_TOKEN_REGEX: &'static str = "^([\\s\\t]+)";
// static EOF_TOKEN_REGEX: &'static str = r"\0";
// static ENDL_TOKEN_REGEX: &'static str = r"\n";

#[derive(Clone, Debug, PartialEq)]
pub enum IgnoredTokens {
	// EndOfFile,
	// EndOfLine,
	WhiteSpace
}

impl MatchToken for IgnoredTokens {
	fn match_token(text: &str) -> Option<Token> {
		if let Some(token) = Self::match_whitespace(text) {
			return Some(token)
		}
		None
	}
}

impl IgnoredTokens {
	fn match_whitespace(text: &str) -> Option<Token> {
		let space_rx = Regex::new(WHITESPACE_TOKEN_REGEX).unwrap_or_else(|e| {
			panic!("Invalid regex for whitespace tokens: {:?}. Error {:?}", WHITESPACE_TOKEN_REGEX, e);
		});

		match space_rx.find(text) {
			Some((begin, end)) => {
				Some(Token::new(
					TokenMatchPos(begin, end), 
					TokenKind::Ignored(IgnoredTokens::WhiteSpace)
				))
			},
			None => None
		}
	}
}