use std::collections::HashMap;
use lang::syntax::token::token_types::*;
use lang::*;
use error::*;

#[derive(Clone, Debug)]
pub struct ObjectDef {
    name: Name,
    fields: HashMap<Name, Handle>
}

impl ObjectDef {
    #[inline]
    fn new(name: Name, fields: HashMap<Name, Handle>) -> Self {
        ObjectDef {
            name: name,
            fields: fields
        }
    }

    #[inline]
    pub fn get_name(&self) -> Name {
        self.name.clone()
    } 
}

struct ObjectFieldDef {
    name: Name,
    fields: Handle
}

impl ObjectFieldDef {
    #[inline]
    fn new(name: Name, fields: Handle) -> Self {
        ObjectFieldDef {
            name: name,
            fields: fields
        }
    }
}

impl Into<(Name, Handle)> for ObjectFieldDef {
    #[inline]
    fn into(self) -> (Name, Handle) {
        (self.name, self.fields)
    }
}

impl Evaluate for ObjectDef {
    fn eval_with_frame(&self, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
        let values: Result<_, ScriptError> = self.fields.iter().map(|(name, field)| {
            let value = try!(EvalContext::eval_expr_with_frame(field, frame));
            Ok((name.clone(), Value::from(value)))
        }).collect();
        let obj = Object::new(self.name.clone(), try!(values));
        Ok(Value::Obj(obj).into())
    }
}

impl MatchConstruct for ObjectDef {
    fn match_construct(tree: &mut ExprTreeIterator) -> Option<Vec<Handle>> {
        let mut iter = tree.iter();
        let ident = match Keyword::find_obj_token(&mut iter)
            .and_then(|_| Identity::find_identity_token(&mut iter)) 
        {
            Some(ident) => ident,
            None => return None
        };
        if let None = OperatorToken::find_assign_token(&mut iter)
            .and_then(|_| Punctuator::find_open_curly_brace_token(&mut iter)) 
        {
            return None;
        };
        let mut fields = HashMap::new();
        while let Some(field) = match_obj_field(&mut iter) {
            let (name, value) = field.into();
            fields.insert(name, value);
        }
        if let None = Punctuator::find_close_curly_brace_token(&mut iter) {
            return None;
        };
        let obj = ObjectDef::new(ident.clone().into(), fields);
        let obj_handle = Handle::new(Expr::from(obj));
        let mut ret = Vec::new();
        let name = Handle::new(<Expr as From<Name>>::from(ident.into()));
        let name_rel = Handle::new(Expr::from(Relation::Nullary(name)));
        let decl = Handle::new(Expr::from(Relation::Unary(Operator::Let, obj_handle.clone())));
        let assign = Handle::new(Expr::from(Relation::Binary(Operator::Assign, name_rel, obj_handle)));
        ret.push(decl);
        ret.push(assign);
        tree.advance_to(iter);
        Some(ret)
    }
}

fn match_obj_field(tree: &mut ExprTreeIterator) -> Option<ObjectFieldDef> {
    let mut iter = tree.iter();
    let ident = match Identity::find_identity_token(&mut iter) {
        Some(ident) => ident,
        None => return None
    };
    if let None = Punctuator::find_colon_token(&mut iter) {
        return None;
    };
    let name = ident.into();
    let handle = match Relation::match_expr(&mut iter) {
        Some(handle) => handle,
        None => return None
    };
    tree.advance_to(iter);
    Some(ObjectFieldDef::new(name, handle))
}

#[derive(Clone, Debug)]
pub struct DotOperator {
    name: Name
}

impl DotOperator {
    pub fn new(name: Name) -> Self {
        DotOperator { name: name }
    }

    pub fn match_operator(tree: &mut ExprTreeIterator) -> Option<Operator> {
        let mut iter = tree.iter();
        if let None = Punctuator::find_dot_token(&mut iter) {
            return None;
        };
        let name = match Identity::find_identity_token(&mut iter) {
            Some(ident) => ident.into(),
            None => return None
        };
        tree.advance_to(iter);
        let dot = DotOperator::new(name);
        Some(Operator::Dot(dot))
    }
}

impl UnaryFunction for DotOperator {
    fn call(&self, a: &Handle, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
        let lvalue = try!(EvalContext::eval_expr_with_frame(a, frame));
        match lvalue.value {
            Value::Obj(ref obj) => {
                obj.get(&self.name).and_then(|val| Some(val.clone().into())).ok_or_else(|| {
                    SyntaxError::Attr(AttributeError::new(obj.typename(), self.name.clone())).into()
                })
            },
            ref t @ _ => {
                // TODO should be a TypeError
                let msg = format!("Dot operator for `{:?}` is not yet implemented.", t);
                Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
            }
        }   
    }
}



