use lang::*;
use error::*;
use std::borrow::Borrow;

#[derive(Clone, Debug)]
pub enum Relation {
	Binary(Operator, Handle, Handle),
	Unary(Operator, Handle),
	Nullary(Handle)
}

impl Evaluate for Relation {
    fn eval_with_frame(&self, stack: &mut StackFrame) -> Result<LValue, ScriptError> {
        match *self {
            Relation::Binary(ref op, ref a, ref b) => {
                (op as &BinaryFunction).call(a, b, stack)
            },
            Relation::Unary(ref op, ref a) => {
                (op as &UnaryFunction).call(a, stack)
            },
            Relation::Nullary(ref a) => match (***a).borrow().kind {
                ExprKind::Name(ref name) => match stack.get_mut(name) {
                    Some(ref value) => Ok(LValue::new(Some(name.clone()), (**value).clone().into())),
                    None => Err(ScriptError::from(ReferenceError::NotDeclared((name.clone()).into())))
                },
                ExprKind::Lit(ref lit) => Ok(LValue::new(None, Value::from(lit.clone()))),
                ref e @ _ => {
                    let msg = format!("Case `{:?}` is not yet implemented.", e);
                    Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
                },
            }
        }
    }
}

pub trait BinaryFunction {
    fn call(&self, &Handle, &Handle, &mut StackFrame) -> Result<LValue, ScriptError>;
}

pub trait UnaryFunction {
    fn call(&self, &Handle, &mut StackFrame) -> Result<LValue, ScriptError>;
}

impl<'a> Relation {
    fn match_unary_prefix_rel(tree: &mut ExprTreeIterator) -> Option<Handle> {
        let mut iter = tree.iter();
        if let Some(operator) = Operator::match_operator(&mut iter) {
            if let Some(expr) = Self::match_nullary_rel(&mut iter) {
                tree.advance_to(iter);
                return Some(Handle::new(Expr::from(Relation::Unary(operator, expr)))); 
            }
        }
        None
    }

    fn match_unary_postfix_rel(tree: &mut ExprTreeIterator) -> Option<Handle> {
        let mut iter = tree.iter();
        if let Some(expr) = Self::match_nullary_rel(&mut iter) {
            if let Some(operator) = Operator::match_operator(&mut iter) {
                tree.advance_to(iter);
                return Some(Handle::new(Expr::from(Relation::Unary(operator, expr)))); 
            }    
        }
        None
    }

    fn match_nullary_rel(tree: &mut ExprTreeIterator) -> Option<Handle> {
        let mut iter = tree.iter();
        let mut rel = None;
        if let Some(token) = iter.next() {
            rel = match token.kind {
                TokenKind::Identity(ref ident) => Some(<Expr as From<Name>>::from(ident.clone().into())),
                TokenKind::Literal(_) => Some(Expr::from(Lit::from((*token).clone()))),
                _ => None
            };
        }
        rel.and_then(|rel| { 
            tree.advance_to(iter);
            let ret = Relation::Nullary(Handle::new(rel));
            Some(Handle::new(Expr::from(ret))) 
        })
    }

    fn match_binary_infix_rel(tree: &mut ExprTreeIterator) -> Option<Handle> {
        let mut iter = tree.iter();
        if let Some(a) = Self::match_nullary_rel(&mut iter) {
            if let Some(operator) = Operator::match_operator(&mut iter) {
                match operator {
                    Operator::Call(_) | Operator::Dot(_) | Operator::Index(_) => return None,
                    _ => {}
                };
                if let Some(b) = Self::match_nullary_rel(&mut iter) {
                    tree.advance_to(iter);
                    return Some(Handle::new(Expr::from(Relation::Binary(operator, a, b))));
                }
            }    
        }
        None
    }
}

impl MatchExpr for Relation {
    fn match_expr(tree: &mut ExprTreeIterator) -> Option<Handle> {
        Self::match_binary_infix_rel(tree)
            .or_else(|| Self::match_unary_postfix_rel(tree))
            .or_else(|| Self::match_nullary_rel(tree))
            .or_else(|| Self::match_unary_prefix_rel(tree))
    }
}


