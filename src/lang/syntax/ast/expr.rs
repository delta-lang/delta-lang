use lang::*;
use error::*;

#[derive(Clone, Debug)]
pub struct Expr {
    pub kind: ExprKind, 
    lrkind: LRValueKind 
}

#[derive(Clone, Debug)]
pub enum LRValueKind {
    LValue,
    RValue
}

#[derive(Clone, Debug)]
pub enum ExprKind {
	Rel(Relation),
	Name(Name),
    Lit(Lit),
    FnDef(FnDef),
    Closure(Closure),
    Obj(ObjectDef),
    Arr(ArrayDef),
}

impl Expr {
	pub fn new(kind: ExprKind, lrkind: LRValueKind) -> Self {
		Expr { kind: kind, lrkind: lrkind }
	}

    pub fn is_lvalue(&self) -> bool {
        match self.lrkind {
            LRValueKind::LValue => true,
            _ => false
        }
    }

    pub fn is_rvalue(&self) -> bool {
        match self.lrkind {
            LRValueKind::RValue => true,
            _ => false
        }
    }

    pub fn as_rvalue(&self) -> Result<RValue, ScriptError> {
        match self.kind {
            ExprKind::Lit(ref lit) => {
                Ok(RValue::new(lit.clone()))
            },
            _ => {
                Err(ScriptError::from(SyntaxError::Assignment("Expression is not an rvalue.".to_owned())))
            }
        }
    }
}

impl From<Lit> for Expr {
    fn from(lit: Lit) -> Self {
        Expr::new(ExprKind::Lit(lit), LRValueKind::RValue)
    }
}

impl From<Name> for Expr {
	fn from(name: Name) -> Self {
		Expr::new(ExprKind::Name(name), LRValueKind::LValue)
	}
}

impl From<FnDef> for Expr {
    fn from(fn_def: FnDef) -> Self {
        Expr::new(ExprKind::FnDef(fn_def), LRValueKind::LValue)
    }
}

impl From<Closure> for Expr {
    fn from(closure: Closure) -> Self {
        Expr::new(ExprKind::Closure(closure), LRValueKind::LValue)
    }
}

impl From<Relation> for Expr {
	fn from(rel: Relation) -> Self {
		Expr::new(ExprKind::Rel(rel), LRValueKind::LValue)
	}
}

impl From<ObjectDef> for Expr {
    fn from(obj: ObjectDef) -> Self {
        Expr::new(ExprKind::Obj(obj), LRValueKind::LValue)
    }
}

impl From<ArrayDef> for Expr {
    fn from(arr: ArrayDef) -> Self {
        Expr::new(ExprKind::Arr(arr), LRValueKind::LValue)
    }
}