use lang::*;
use error::*;

#[derive(Clone, Debug)]
pub struct Closure {
	locals: Vec<Name>,
	enclosed: Handle
}

impl Closure {
	pub fn new(locals: Vec<Name>, enclosed: Handle) -> Self {
		Closure {
			locals: locals,
			enclosed: enclosed
		}
	}
}

impl Evaluate for Closure {
    fn eval_with_frame(&self, _: &mut StackFrame) -> Result<LValue, ScriptError> {
    	let function = Function::Internal(self.clone());
    	Ok(Value::Function(function).into())
    }
}

impl CallAsFunction for Closure {
	fn call(&self, args: Arguments, _: &mut StackFrame) -> Result<Value, ScriptError> {
		let locals = self.locals.iter().zip(args.iter()).map(|(name, value)| { 
			(name.clone(), StackValue::from(value.clone()))
		}).collect();
		let mut frame = StackFrame::new_with_locals(locals);
		let lval = try!(EvalContext::eval_expr_with_frame(&self.enclosed, &mut frame));
		Ok(Value::from(lval))
	}
}
