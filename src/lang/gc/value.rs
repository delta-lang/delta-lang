use lang::*;
use std::rc::Rc;
use std::cell::RefCell;
use std::ops::Deref;

#[derive(Clone, Debug)]
pub struct ValueHandle(Rc<RefCell<Value>>);

impl ValueHandle {
	pub fn new(value: Value) -> Self {
		ValueHandle(Rc::new(RefCell::new(value)))
	}
}

impl Deref for ValueHandle {
	type Target = Rc<RefCell<Value>>;
    fn deref(&self) -> &Self::Target {
    	&self.0
    }
}

#[derive(Clone, Debug)]
pub enum StackValue {
	// TODO Captured(WeakValueHandle),
	Local(ValueHandle)
}

impl From<Value> for StackValue {
    fn from(value: Value) -> StackValue {
    	StackValue::Local(ValueHandle::new(value))
    }
}

impl Into<Value> for StackValue {
	fn into(self) -> Value {
		match self {
			StackValue::Local(handle) => (&*handle).borrow().clone()
		}
	}
}