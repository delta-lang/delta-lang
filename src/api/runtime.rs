use lang::*;
use error::*;
use api::*;

pub struct Runtime {
	ctx: EvalContext
}

impl Runtime {
    pub fn new() -> Self {
        Runtime {
            ctx: EvalContext::new()
        }
    }

    pub fn load_package(&mut self, pkg: Package) {
        self.ctx.load_package(pkg);
    }

    pub fn eval_unit(&mut self, mut script: ScriptUnit) -> Result<Vec<Value>, ScriptError> {
        let mut ret = Vec::new();
        while let Some(ref expr) = script.ast.pop() {
            let lval = try!(self.ctx.eval_expr(expr));
            ret.push(Value::from(lval));
        }
        Ok(ret)
    }
}