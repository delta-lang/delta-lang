use lang::*;

#[derive(Debug, Clone)]
pub enum Value {
	Lit(Lit),
	Package(Package),
	Function(Function),
	Obj(Object),
	Arr(Array),
	Undefined
}

impl Typename for Value {
	fn typename(&self) -> Name {
		match *self {
			Value::Undefined => Name::new("Undefined".to_owned()),
			Value::Lit(ref item) => (item as &Typename).typename(),
			Value::Package(ref item) => (item as &Typename).typename(),
			Value::Function(ref item) => (item as &Typename).typename(),
			Value::Obj(ref item) => (item as &Typename).typename(),
			Value::Arr(ref item) => (item as &Typename).typename(),
		}
	}
}

impl AsStr for Value {
	fn as_str(&self) -> Str {
		match *self {
			Value::Undefined => Str::new("Undefined".to_owned()),
			Value::Lit(ref item) => (item as &AsStr).as_str(),
			Value::Package(ref item) => (item as &AsStr).as_str(),
			Value::Function(ref item) => (item as &AsStr).as_str(),
			Value::Obj(ref item) => (item as &AsStr).as_str(),
			Value::Arr(ref item) => (item as &AsStr).as_str(),
		}
	}
}

impl From<LValue> for Value {	
	fn from(lval: LValue) -> Value {
		lval.value
	}
}

impl From<Lit> for Value {	
	fn from(lit: Lit) -> Value {
		Value::Lit(lit)
	}
}

#[derive(Debug, Clone)]
pub struct LValue {
	pub name: Option<Name>,
	pub value: Value,
}

impl LValue {
	#[inline]
    pub fn new(name: Option<Name>, value: Value) -> Self {
    	LValue {
    		name: name,
    		value: value
    	}
    }
}

impl Into<LValue> for Value {	
	fn into(self) -> LValue {
		LValue::new(None, self)
	}
}

#[derive(Debug)]
pub struct RValue {
	value: Value,
}

impl RValue {
    pub fn new(lit: Lit) -> Self {
    	RValue {
    		value: Value::from(lit)
    	}
    }
}

impl Into<Value> for RValue {
	fn into(self) -> Value {
		self.value
	}
}