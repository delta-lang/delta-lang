use lang::*;
use lang::syntax::token::token_types::*;
use error::*;

#[derive(Clone, Debug)]
pub struct IndexOperator {
    handle: Handle
}

impl IndexOperator {
    pub fn new(handle: Handle) -> Self {
        IndexOperator { handle: handle }
    }

    pub fn match_operator(tree: &mut ExprTreeIterator) -> Option<Operator> {
        let mut iter = tree.iter();
        if let None = Punctuator::find_open_square_brace_token(&mut iter) {
            return None;
        };
        let index = match Relation::match_expr(&mut iter) {
            Some(rel) => rel.into(),
            None => return None
        };
        if let None = Punctuator::find_close_square_brace_token(&mut iter) {
            return None;
        };
        tree.advance_to(iter);
        let operator = IndexOperator::new(index);
        Some(Operator::Index(operator))
    }
}

impl UnaryFunction for IndexOperator {
    fn call(&self, a: &Handle, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
        let lvalue = try!(EvalContext::eval_expr_with_frame(a, frame));
        let idx_lvalue = try!(EvalContext::eval_expr_with_frame(&self.handle, frame));
        let idx = match idx_lvalue.value {
            Value::Lit(Lit::Num(ref num)) => num.clone(),
            ref t @ _ => {
                let err = IndexError::WrongIndexType(t.clone());
                return Err(SyntaxError::Index(err).into());
            }
        };
        match lvalue.value {
            Value::Arr(ref arr) => {
                arr.get(idx.clone()).and_then(|val| Some(val.clone().into())).ok_or_else(|| {
                    let err = IndexError::OutOfBounds(idx);
                    SyntaxError::Index(err).into()
                })
            },
            ref t @ _ => {
                // TODO should be a TypeError
                let msg = format!("Dot operator for `{:?}` is not yet implemented.", t);
                Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
            }
        }
    }
}