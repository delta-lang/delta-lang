use lang::*;
use lang::syntax::token::token_types::*;

pub struct LetClause;

impl LetClause {
	fn match_let_keyword<'a, I: Iterator<Item=&'a Token>>(iter: &mut I) -> Option<()> {
		if let Some(token) = iter.next() {
            return match token.kind {
                TokenKind::Keyword(ref word) if *word == Keyword::Let => Some(()),
                _ => None,
            }
        }
        None
	}

	fn match_identity<'a, I: Iterator<Item=&'a Token>>(iter: &mut I) -> Option<Name> {
		if let Some(token) = iter.next() {
            return match token.kind {
                TokenKind::Identity(ref ident) => Some(ident.clone().into()),
                _ => None,
            }
        }
        None
	}

	fn match_name(tree: &mut ExprTreeIterator) -> Option<Name> {
		Self::match_let_keyword(tree).and_then(|_| Self::match_identity(tree))
	}
}

impl MatchConstruct for LetClause {
    fn match_construct(tree: &mut ExprTreeIterator) -> Option<Vec<Handle>> {
        let mut iter = tree.iter();
        let name = match Self::match_name(&mut iter) {
            Some(name) => name,
            None => return None
        };
        let _ = iter.next();
        let rel = match Relation::match_expr(&mut iter) {
            Some(rel) => rel,
            None => return None
        };
        let mut ret = Vec::new();
        let name_handle = Handle::new(Expr::from(name));
        let decl = Handle::new(Expr::from(Relation::Unary(Operator::Let, name_handle.clone())));
        let name_rel = Handle::new(Expr::from(Relation::Nullary(name_handle)));
        let assign = Handle::new(Expr::from(Relation::Binary(Operator::Assign, name_rel, rel)));
        ret.push(decl);
        ret.push(assign);
        tree.advance_to(iter);
        Some(ret)
    }
}
