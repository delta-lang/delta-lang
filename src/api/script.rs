use lang::*;
use error::*;

#[derive(Debug)]
pub struct ScriptUnit {
    pub ast: Ast
}

impl ScriptUnit {
    pub fn new<'a>(text: &'a str) -> Result<Self, ScriptError> {
        let tokens = try!(Parser::tokenize(text).map_err(|e| ScriptError::from(e)));
        let ast = try!(Parser::parse_tokens(tokens));
        Ok(ScriptUnit { ast: ast })
    }
}