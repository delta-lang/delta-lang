use lang::*;

pub trait Typename {
	fn typename(&self) -> Name;
}