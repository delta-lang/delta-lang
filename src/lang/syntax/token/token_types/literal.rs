use regex::Regex;
use lang::*;

static NUMERIC_TOKEN_REGEX: &'static str = r"^(\d+(\.\d+)?)";
static STRING_TOKEN_REGEX: &'static str = "^(\"(?:\\.|[^\"\\\\])*\")";

// TODO rename to LiteralToken to be distinct from the Lit type
#[derive(Clone, Debug, PartialEq)]
pub enum Literal {
    Number(String),
    String(String)
}

impl Literal {
	pub fn new_number(text: String) -> Self {
		Literal::Number(text)
	}

	pub fn new_string(text: String) -> Self {
		Literal::String(text)
	}
}

impl MatchToken for Literal {
	fn match_token(text: &str) -> Option<Token> {
		Self::match_number(text)
			.or_else(|| Self::match_string(text))
	}
}

impl Literal {
	fn match_number(text: &str) -> Option<Token> {
		let number_rx = Regex::new(NUMERIC_TOKEN_REGEX).unwrap_or_else(|e| {
			panic!("Invalid regex for Numeric tokens: {:?}. Error {:?}", NUMERIC_TOKEN_REGEX, e);
		});

		match number_rx.find(text) {
			Some((begin, end)) => {
				Some(Token::new(
					TokenMatchPos(begin, end), 
					TokenKind::Literal(Literal::new_number(text[begin..end].to_owned()))
				))
			},
			None => None
		}
	}

	fn match_string(text: &str) -> Option<Token> {
		let str_rx = Regex::new(STRING_TOKEN_REGEX).unwrap_or_else(|e| {
			panic!("Invalid regex for String tokens: {:?}. Error {:?}", STRING_TOKEN_REGEX, e);
		});

		match str_rx.find(text) {
			Some((begin, end)) => {
				Some(Token::new(
					TokenMatchPos(begin, end), 
					TokenKind::Literal(Literal::new_string(text[(begin+1)..(end-1)].to_owned()))
				))
			},
			None => None
		}
	}
}