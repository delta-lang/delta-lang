use regex::Regex;
use lang::*;

static PUNC_SEMICOLON_TOKEN_REGEX: &'static str = "^([;])";
static PUNC_OPEN_PAREN_TOKEN_REGEX: &'static str = "^([(])";
static PUNC_CLOSE_PAREN_TOKEN_REGEX: &'static str = "^([)])";
static PUNC_OPEN_CURLY_BRACE_TOKEN_REGEX: &'static str = "^([{])";
static PUNC_CLOSE_CURLY_BRACE_TOKEN_REGEX: &'static str = "^([}])";
static PUNC_COLON_TOKEN_REGEX: &'static str = "^([:])";
static PUNC_DOT_TOKEN_REGEX: &'static str = "^([.])";
static PUNC_OPEN_SQUARE_BRACE_TOKEN_REGEX: &'static str = "^([\\[])";
static PUNC_CLOSE_SQUARE_BRACE_TOKEN_REGEX: &'static str = "^([\\]])";
static PUNC_COMMA_TOKEN_REGEX: &'static str = "^([,])";

#[derive(Debug, Clone, PartialEq)]
pub enum Punctuator {
	Dot,
	Colon,
	Comma,
	Semicolon,
	OpenParen,
	CloseParen,
	OpenCurlyBrace, 
	CloseCurlyBrace,
	OpenSquareBrace, 
	CloseSquareBrace,
}

impl Punctuator {
	pub fn find_open_curly_brace_token(tree: &mut ExprTreeIterator) -> Option<Punctuator> {
		match tree.next() {
			Some( &Token { kind: TokenKind::Punctuator(ref kind @ _), .. }) 
				if *kind == Punctuator::OpenCurlyBrace => Some(kind.clone()),
			_ => None,
		}
	}

	pub fn find_close_curly_brace_token(tree: &mut ExprTreeIterator) -> Option<Punctuator> {
		match tree.next() {
			Some( &Token { kind: TokenKind::Punctuator(ref kind @ _), .. }) 
				if *kind == Punctuator::CloseCurlyBrace => Some(kind.clone()),
			_ => None,
		}
	}

	pub fn find_colon_token(tree: &mut ExprTreeIterator) -> Option<Punctuator> {
		match tree.next() {
			Some( &Token { kind: TokenKind::Punctuator(ref kind @ _), .. }) 
				if *kind == Punctuator::Colon => Some(kind.clone()),
			_ => None,
		}
	}

	pub fn find_dot_token(tree: &mut ExprTreeIterator) -> Option<Punctuator> {
		match tree.next() {
			Some( &Token { kind: TokenKind::Punctuator(ref kind @ _), .. }) 
				if *kind == Punctuator::Dot => Some(kind.clone()),
			_ => None,
		}
	}

	pub fn find_open_square_brace_token(tree: &mut ExprTreeIterator) -> Option<Punctuator> {
		match tree.next() {
			Some( &Token { kind: TokenKind::Punctuator(ref kind @ _), .. }) 
				if *kind == Punctuator::OpenSquareBrace => Some(kind.clone()),
			_ => None,
		}
	}

	pub fn find_close_square_brace_token(tree: &mut ExprTreeIterator) -> Option<Punctuator> {
		let mut iter = tree.iter();
		match iter.next() {
			Some( &Token { kind: TokenKind::Punctuator(ref kind @ _), .. }) 
				if *kind == Punctuator::CloseSquareBrace => {
					tree.advance_to(iter);
					Some(kind.clone())
				},
			_ => None,
		}
	}

	pub fn find_comma_token(tree: &mut ExprTreeIterator) -> Option<Punctuator> {
		let mut iter = tree.iter();
		match iter.next() {
			Some( &Token { kind: TokenKind::Punctuator(ref kind @ _), .. }) 
				if *kind == Punctuator::Comma => {
					tree.advance_to(iter);
					Some(kind.clone())
				},
			_ => None,
		}
	}
}

impl MatchToken for Punctuator {
	fn match_token(text: &str) -> Option<Token> {
		match_semicolon_token_in_text(text)
			.or_else(|| match_open_paren_token_in_text(text))
			.or_else(|| match_close_paren_token_in_text(text))
			.or_else(|| match_open_curly_brace_token_in_text(text))
			.or_else(|| match_close_curly_brace_token_in_text(text))
			.or_else(|| match_colon_token_in_text(text))
			.or_else(|| match_dot_token_in_text(text))
			.or_else(|| match_open_square_brace_token_in_text(text))
			.or_else(|| match_close_square_brace_token_in_text(text))
			.or_else(|| match_comma_token_in_text(text))
	}
}


fn match_semicolon_token_in_text(text: &str) -> Option<Token> {
	let semic_rx = Regex::new(PUNC_SEMICOLON_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::Semicolon: {:?}. Error {:?}", PUNC_SEMICOLON_TOKEN_REGEX, e);
	});

	match semic_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::Semicolon)
			))
		},
		None => None
	}
}

fn match_open_paren_token_in_text(text: &str) -> Option<Token> {
	let open_rx = Regex::new(PUNC_OPEN_PAREN_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::OpenParen: {:?}. Error {:?}", PUNC_OPEN_PAREN_TOKEN_REGEX, e);
	});

	match open_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new( 
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::OpenParen)
			))
		},
		None => None
	}
}

fn match_close_paren_token_in_text(text: &str) -> Option<Token> {
	let close_rx = Regex::new(PUNC_CLOSE_PAREN_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::CloseParen: {:?}. Error {:?}", PUNC_CLOSE_PAREN_TOKEN_REGEX, e);
	});

	match close_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::CloseParen)
			))
		},
		None => None
	}
}

fn match_close_curly_brace_token_in_text(text: &str) -> Option<Token> {
	let close_rx = Regex::new(PUNC_CLOSE_CURLY_BRACE_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::CloseCurlyBrace: {:?}. Error {:?}", PUNC_CLOSE_CURLY_BRACE_TOKEN_REGEX, e);
	});

	match close_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::CloseCurlyBrace)
			))
		},
		None => None
	}
}

fn match_open_curly_brace_token_in_text(text: &str) -> Option<Token> {
	let open_rx = Regex::new(PUNC_OPEN_CURLY_BRACE_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::OpenCurlyBrace: {:?}. Error {:?}", PUNC_OPEN_CURLY_BRACE_TOKEN_REGEX, e);
	});

	match open_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::OpenCurlyBrace)
			))
		},
		None => None
	}
}

fn match_colon_token_in_text(text: &str) -> Option<Token> {
	let colon_rx = Regex::new(PUNC_COLON_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::Colon: {:?}. Error {:?}", PUNC_COLON_TOKEN_REGEX, e);
	});

	match colon_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new( 
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::Colon)
			))
		},
		None => None
	}
}

fn match_dot_token_in_text(text: &str) -> Option<Token> {
	let dot_rx = Regex::new(PUNC_DOT_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::Dot: {:?}. Error {:?}", PUNC_DOT_TOKEN_REGEX, e);
	});

	match dot_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new( 
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::Dot)
			))
		},
		None => None
	}
}

fn match_close_square_brace_token_in_text(text: &str) -> Option<Token> {
	let close_rx = Regex::new(PUNC_CLOSE_SQUARE_BRACE_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::CloseSquareBrace: {:?}. Error {:?}", PUNC_CLOSE_SQUARE_BRACE_TOKEN_REGEX, e);
	});

	match close_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::CloseSquareBrace)
			))
		},
		None => None
	}
}

fn match_open_square_brace_token_in_text(text: &str) -> Option<Token> {
	let open_rx = Regex::new(PUNC_OPEN_SQUARE_BRACE_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::OpenSquareBrace: {:?}. Error {:?}", PUNC_OPEN_SQUARE_BRACE_TOKEN_REGEX, e);
	});

	match open_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::OpenSquareBrace)
			))
		},
		None => None
	}
}

fn match_comma_token_in_text(text: &str) -> Option<Token> {
	let open_rx = Regex::new(PUNC_COMMA_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Punctuator::Comma: {:?}. Error {:?}", PUNC_COMMA_TOKEN_REGEX, e);
	});

	match open_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new(
				TokenMatchPos(begin, end), 
				TokenKind::Punctuator(Punctuator::Comma)
			))
		},
		None => None
	}
}