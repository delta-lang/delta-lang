use std::str::FromStr;
use lang::*;
use lang::syntax::token::token_types::*;

#[derive(Clone, Debug, PartialEq)]
pub enum Lit {
	Num(Number),
	Str(Str)
}

impl Typename for Lit {
	fn typename(&self) -> Name {
		match *self {
			Lit::Num(ref item) => (item as &Typename).typename(),
			Lit::Str(ref item) => (item as &Typename).typename()
		}
	}
}

impl AsStr for Lit {
	fn as_str(&self) -> Str {
		match *self {
		    Lit::Num(ref item) => (item as &AsStr).as_str(),
			Lit::Str(ref item) => item.clone()
		}
	}
}

impl From<Number> for Lit {
	fn from(num: Number) -> Self {
		Lit::Num(num)
	}
}

impl From<Str> for Lit {
	fn from(string: Str) -> Self {
		Lit::Str(string)
	}
}

impl From<Token> for Lit {
	fn from(token: Token) -> Self {
		match token.kind {
			TokenKind::Literal(lit) => match lit {
				Literal::Number(ref text) => Lit::from(Number::from(text.clone())),
				Literal::String(ref text) => Lit::from(Str::from(text.clone()))
			},
			_ => unreachable!()
		}
	}
}

#[derive(Clone, Debug, PartialEq)]
pub struct Number(f64);

impl Number {
	pub fn new(n: f64) -> Self {
		Number(n)
	}

	#[inline]
	pub fn value(&self) -> f64 {
		self.0
	}

	#[inline]
	pub fn as_usize(&self) -> usize {
		self.0 as usize
	}
}

impl Typename for Number {
	fn typename(&self) -> Name {
		Name::new("Number".to_owned())
	}
}

impl AsStr for Number {
	fn as_str(&self) -> Str {
		let n = format!("{:?}", self.0);
		Str::new(n)	
	}
}

impl From<String> for Number {
	fn from(string: String) -> Self {
		Number(f64::from_str(&*string).unwrap())
	}
}

#[derive(Clone, Debug, PartialEq)]
pub struct Str(String);

impl Str {
	#[inline]
	pub fn new(string: String) -> Self {
		Str(string)
	}

	#[inline]
	pub fn value(&self) -> String {
		self.0.clone()
	}
}

impl From<String> for Str {
	fn from(string: String) -> Self {
		Str::new(string)
	}
}

impl From<Str> for String {
	fn from(string: Str) -> String {
		string.value()
	}
}

impl From<Name> for Str {
	fn from(name: Name) -> Self {
		Str::new(name.into())
	}
}

impl Typename for Str {
	fn typename(&self) -> Name {
		Name::new("Str".to_owned())
	}
}