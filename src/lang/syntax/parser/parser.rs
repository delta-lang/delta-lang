use lang::syntax::token::token_types::*;
use lang::*;
use std::collections::VecDeque;
use std::iter::FromIterator;

pub struct Parser;

impl Parser {
    pub fn tokenize<'a>(text: &'a str) -> Result<Vec<Token>, SyntaxError> {
		let mut iter = TokenizerIterator::new(text);
		let mut tokens = Vec::new();
    	for token in iter.by_ref() {
    		match token.kind {
    			TokenKind::Comment(_) | TokenKind::Ignored(_) => continue,
    			_ => {}
    		};
			tokens.push(token);
		}
		if iter.0.len() > 0 {
			return Err(SyntaxError::InvalidCharacters(
				InvalidCharactersError::new(iter.0.to_owned())
			));
		}
    	Ok(tokens)
    }

    pub fn parse_tokens(vec: Vec<Token>) -> Result<Ast, SyntaxError> {
    	let mut ast = Ast::new();
    	let mut tokens = VecDeque::from_iter(vec.iter().cloned());

    	loop {
    		let positions;

    		{
    			/* 

    			The anonymous closure wrapping the if-let helps to get away with mutating `tokens` with information from the matched expression.
    			This is necessary because currently Rust won't allow you to mutably borrow within an if-let clause if the value has been borrowed
    			as part of the condition.

    			See https://github.com/rust-lang/rust/issues/6393.

				- Jon Brennecke (1/29/16)

    			*/

				let mut iter = ExprTreeIterator::new(&tokens, 0);
				match Self::match_construct(&mut iter) {
					Some(handles) => {
						ast.extend(handles.iter().cloned());
						positions = iter.position;
					},
				    None => match Relation::match_expr(&mut iter) {
				    	Some(handle) => {
				    		ast.push(handle);
				    		positions = iter.position;
				    	},
				    	None => break
				    },
				}
			}

			for _ in 0..positions {
	            tokens.pop_front();
	        }
		}

		if let Some(token) = tokens.pop_front() {
			return Err(SyntaxError::UnexpectedToken(UnexpectedTokenError::new(token.clone())));
		}

		Ok(ast)
	}
}

impl MatchConstruct for Parser {
	fn match_construct(tree: &mut ExprTreeIterator) -> Option<Vec<Handle>> {
		LetClause::match_construct(tree)
			.or_else(|| FnConstruct::match_construct(tree))
			.or_else(|| ObjectDef::match_construct(tree))
			.or_else(|| ArrayDef::match_construct(tree))
	}
}

pub struct ExprTreeIterator<'a> {
	tokens: &'a VecDeque<Token>,
	position: usize
}

impl<'a> ExprTreeIterator<'a> {
	pub fn new(tokens: &'a VecDeque<Token>, position: usize) -> Self {
		ExprTreeIterator {
			tokens: tokens,
			position: position
		}
	}

	pub fn advance_to(&mut self, iter: ExprTreeIterator) {
		self.position = iter.position;
	}

	pub fn iter(&self) -> ExprTreeIterator<'a> {
    	ExprTreeIterator::new(self.tokens, self.position)
    }
}

impl<'a> Iterator for ExprTreeIterator<'a> {
    type Item = &'a Token;

    fn next(&mut self) -> Option<Self::Item> {
    	if self.position < self.tokens.len() {
    		let token = &self.tokens[self.position];
    		self.position += 1;
	    	Some(token)
    	}
    	else {
    		return None;
    	}
    }
}



pub struct TokenizerIterator<'a>(&'a str);

impl<'a> TokenizerIterator<'a> {
	fn new(text: &'a str) -> Self {
		TokenizerIterator(text)
	}

	fn consume_text(&mut self, pos: TokenMatchPos) {
		let (_, end) = pos.into();
		self.0 = &self.0[end..];
	}
}

impl<'a> Iterator for TokenizerIterator<'a> {
	type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
    	Keyword::match_token(self.0)
    		.or(Comment::match_token(self.0))
			.or(Literal::match_token(self.0))
			.or(IgnoredTokens::match_token(self.0))
			.or(OperatorToken::match_token(self.0))
			.or(Identity::match_token(self.0))
			.or(Punctuator::match_token(self.0))
			.and_then(|token| {
				self.consume_text(token.pos);
				Some(token)
			})
    }
}

pub trait MatchConstruct {
    fn match_construct(tree: &mut ExprTreeIterator) -> Option<Vec<Handle>>;
}

pub trait MatchExpr {
	fn match_expr(&mut ExprTreeIterator) -> Option<Handle>;
}

pub trait MatchToken {
	fn match_token(&str) -> Option<Token>;
}
