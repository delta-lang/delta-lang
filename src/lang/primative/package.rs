use lang::*;
use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct Package {
    pub name: Name,
    exports: HashMap<Name, Value>
}

impl Package {
	pub fn new(name: Name) -> Self {
		Package {
			name: name,
			exports: HashMap::new()
		}
	}

	pub fn set(&mut self, name: Name, value: Value) {
		self.exports.insert(name, value);
	}

	pub fn import_into_scope(&self, scope: &mut StackFrame) {
		for (name, value) in self.exports.iter() {
			scope.set(name.clone(), value.clone());
		}
	}
}

impl Typename for Package {
	fn typename(&self) -> Name {
		Name::new("Package".to_owned())
	}
}

impl AsStr for Package {
	fn as_str(&self) -> Str {
	    Str::from(self.name.clone())
	}
}