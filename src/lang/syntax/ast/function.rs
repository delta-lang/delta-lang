use lang::*;
use lang::syntax::token::token_types::*;
use error::*;

#[derive(Debug, Clone)]
pub struct CallOperator {
    pub args: Vec<Handle>
}

impl CallOperator {
    pub fn new(tree: &mut ExprTreeIterator) -> Option<CallOperator> {  
        let mut iter = tree.iter();
        let mut args = Vec::new();
        let mut size = 0;

        if let Some(token) = iter.next() {
            match token.kind {
                TokenKind::Punctuator(ref p) if *p == Punctuator::OpenParen => size += 1,
                _ => return None
            }
        }

        while let Some(token) = iter.next() {
            if let Some(arg) = match token.kind {
                TokenKind::Identity(ref ident) => Some(<Expr as From<Name>>::from(ident.clone().into())),
                TokenKind::Literal(_) => Some(Expr::from(Lit::from((*token).clone()))),
                _ => None,
            } {
                args.push(Handle::new(arg));
            }
            else {
                match token.kind {
                    TokenKind::Punctuator(ref p) if *p == Punctuator::CloseParen => { 
                        size += 1;
                        break; 
                    },
                    _ => return None
                }
            }
            size += 1;
        }

        if size < 2 {
            return None;
        }

        tree.advance_to(iter);

        Some(CallOperator {
            args: args
        })
    }

    fn call_name_with_frame(&self, name: Name, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
        let stack_value = match frame.get_mut(&name) {
            Some(stack_value) => stack_value.clone(),
            _ => return Err(ScriptError::from(ReferenceError::NotDeclared(name.into()))),   
        };

        match stack_value.into() {
            Value::Function(ref fun) => {
                let args = try!(Arguments::new_from_call_operator(self.clone(), frame));
                fun.call(args, frame).and_then(|res| Ok(res.into()))
            },
            ref e @ _ => {
                let msg = format!("Call operator for `{:?}` is not yet supported.", e);
                Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
            },  
        }
    }
}

impl UnaryFunction for CallOperator {
    fn call(&self, a: &Handle, frame: &mut StackFrame) -> Result<LValue, ScriptError> {
        match a.borrow().kind {
            ExprKind::Name(ref name) => self.call_name_with_frame(name.clone(), frame),
            ExprKind::Rel(ref rel) => {
                let lval = try!(rel.eval_with_frame(frame));
                match lval.name {
                    Some(ref name) => self.call_name_with_frame(name.clone(), frame),
                    None => {
                        let received = format!("{:?}", lval.value);
                        let msg = format!("Cannot call `{:?}` as a function.", lval.value);
                        Err(ScriptError::from(SyntaxError::Type(TypeError::new("Function".to_owned(), received, msg))))
                    },
                }
            },
            ref e @ _ => {
                let msg = format!("Call operator for `{:?}` is not yet supported.", e);
                Err(ScriptError::from(SyntaxError::Unimplemented(msg)))
            } 
        }
    }
}

#[derive(Debug, Clone)]
pub struct FnDef {
    name: Name,
    args: Vec<Name>
}

impl FnDef {
    
    #[inline]
    pub fn get_name(&self) -> Name {
        self.name.clone()
    }

    #[inline]
    pub fn get_args(&self) -> Vec<Name> {
        self.args.clone()
    }

    fn match_fn_keyword(tree: &mut ExprTreeIterator) -> Option<()> {
        if let Some(token) = tree.next() {
            return match token.kind {
                TokenKind::Keyword(ref word) if *word == Keyword::Fn => Some(()),
                _ => None,
            }
        }
        None
    }

    fn match_identity(tree: &mut ExprTreeIterator) -> Option<Name> {
        if let Some(token) = tree.next() {
            return match token.kind {
                TokenKind::Identity(ref ident) => Some(ident.clone().into()),
                _ => None,
            }
        }
        None
    }

    fn match_arglist(tree: &mut ExprTreeIterator) -> Option<Vec<Name>> {
        let mut iter = tree.iter(); 
        let mut args = Vec::new();
        let mut size = 0;

        if let Some(token) = iter.next() {
            match token.kind {
                TokenKind::Punctuator(ref p) if *p == Punctuator::OpenParen => size += 1,
                _ => return None
            }
        }

        while let Some(token) = iter.next() {
            if let Some(arg) = match token.kind {
                TokenKind::Identity(ref ident) => Some(ident.clone().into()),
                _ => None,
            } {
                args.push(arg);
            }
            else {
                match token.kind {
                    TokenKind::Punctuator(ref p) if *p == Punctuator::CloseParen => { 
                        size += 1;
                        break; 
                    },
                    _ => return None
                }
            }
            size += 1;
        }

        if size < 2 {
            return None;
        }

        tree.advance_to(iter);
        Some(args)
    }

    fn new(name: Name, args: Vec<Name>) -> Self {
        FnDef {
            name: name,
            args: args
        }
    }

    pub fn new_with_expr_tree(tree: &mut ExprTreeIterator) -> Option<FnDef> {
        Self::match_fn_keyword(tree)
            .and_then(|_| Self::match_identity(tree))
            .and_then(|name| {
                let args = match Self::match_arglist(tree) {
                    Some(args) => args,
                    None => return None
                };
                Some(FnDef::new(name, args))
            })
    }
}