use lang::*;
use std::collections::vec_deque::Iter;
use std::collections::VecDeque;

#[derive(Debug)]
pub struct Ast(VecDeque<Handle>);

impl Ast {
	pub fn new() -> Self {
		Ast(VecDeque::new())
	}

	pub fn push(&mut self, handle: Handle) {
		self.0.push_back(handle);
	}

	pub fn pop(&mut self) -> Option<Handle> {
		self.0.pop_front()
	}

	pub fn iter(&self) -> Iter<Handle> {
		self.0.iter()
	}
}

impl Extend<Handle> for Ast {
	fn extend<T>(&mut self, iter: T) where T: IntoIterator<Item=Handle> {
		self.0.extend(iter)
	}
}