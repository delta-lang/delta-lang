use lang::*;

#[derive(Debug)]
pub enum SyntaxError {
    Attr(AttributeError),
    Index(IndexError),
    Type(TypeError),
	Assignment(String),
	Unimplemented(String),
	UnexpectedToken(UnexpectedTokenError),
	InvalidCharacters(InvalidCharactersError)
}

#[derive(Debug)]
pub struct UnexpectedTokenError(Token);

impl UnexpectedTokenError {
    pub fn new(token: Token) -> Self {
    	UnexpectedTokenError(token)
    }
}

#[derive(Debug)]
pub struct InvalidCharactersError(String);

impl InvalidCharactersError {
    pub fn new(chars: String) -> Self {
    	InvalidCharactersError(chars)
    }
}

#[derive(Debug)]
pub struct TypeError {
    expected: String,
    received: String,
    msg: String
}

impl TypeError {
    pub fn new(expected: String, received: String, msg: String) -> Self {
        TypeError {
            expected: expected,
            received: received,
            msg: msg
        }
    }
}

#[derive(Debug)]
pub struct AttributeError {
    obj_name: Name,
    prop_name: Name
}

// object 'obj_name' has no attribute 'prop_name'
impl AttributeError {
    pub fn new(obj_name: Name, prop_name: Name) -> Self {
        AttributeError {
            obj_name: obj_name, 
            prop_name: prop_name
        }
    }
}

#[derive(Debug)]
pub enum IndexError {
    WrongIndexType(Value),
    OutOfBounds(Number)
}