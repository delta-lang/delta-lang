use lang::syntax::token::token_types::*;

#[derive(Debug, Clone)]
pub struct Token {
	pub pos: TokenMatchPos,
	pub kind: TokenKind
}

impl Token {
	pub fn new(pos: TokenMatchPos, kind: TokenKind) -> Self {
		Token {
			pos: pos, 
			kind: kind 
		}
	}
}

#[derive(Debug, Copy, Clone)]
pub struct TokenMatchPos(pub usize, pub usize);

impl Into<(usize, usize)> for TokenMatchPos {
    fn into(self) -> (usize, usize) {
        (self.0, self.1)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum TokenKind {
	Keyword(Keyword),
	Identity(Identity),
	Operator(OperatorToken),
	Ignored(IgnoredTokens),
	Literal(Literal),
	Punctuator(Punctuator),
	Comment(Comment)
}