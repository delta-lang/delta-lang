use lang::*;
use std::io::Error as IoError;

#[derive(Debug)]
pub enum ScriptError {
	Syntax(SyntaxError),
	Internal(InternalError),
	Reference(ReferenceError)
}

impl From<SyntaxError> for ScriptError {
    fn from(err: SyntaxError) -> ScriptError {
    	ScriptError::Syntax(err)
    }
}

impl From<InternalError> for ScriptError {
    fn from(err: InternalError) -> ScriptError {
    	ScriptError::Internal(err)
    }
}

impl From<ReferenceError> for ScriptError {
    fn from(err: ReferenceError) -> ScriptError {
    	ScriptError::Reference(err)
    }
}

#[derive(Debug)]
pub enum InternalError {
	CorruptedStack,
    Io(IoError)
}

impl From<IoError> for InternalError {
    fn from(err: IoError) -> InternalError {
        InternalError::Io(err)
    }
}

#[derive(Debug)]
pub enum ReferenceError {
	NotDeclared(Name)
}