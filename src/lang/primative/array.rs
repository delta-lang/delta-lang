use lang::*;

#[derive(Clone, Debug)]
pub struct Array {
	values: Vec<Value>
}

impl Array {
	pub fn new(values: Vec<Value>) -> Self {
		Array {
			values: values
		}
	}

	#[inline]
	pub fn get(&self, index: Number) -> Option<&Value> {
		let n = index.as_usize();
		self.values.get(n)
	}
}

impl Typename for Array {
	fn typename(&self) -> Name {
		Name::new("Array".to_owned())
	}
}

impl AsStr for Array {
	fn as_str(&self) -> Str {
		Str::new("[ array ]".to_owned())
	}
}