use lang::*;

pub struct FnConstruct;

impl MatchConstruct for FnConstruct {
    fn match_construct(tree: &mut ExprTreeIterator) -> Option<Vec<Handle>> {
		let mut iter = tree.iter();
		let fn_def = match FnDef::new_with_expr_tree(&mut iter) {
			Some(fn_def) => fn_def,
			_ => return None
		};
		let _ = iter.next();
		match Relation::match_expr(&mut iter) {
			Some(fn_body) => {
				let closure = Closure::new(fn_def.get_args(), fn_body);
				let closure_handle = Handle::new(Expr::from(closure));
				let mut ret = Vec::new();
				let name = Handle::new(Expr::from(fn_def.get_name()));
				let name_rel = Handle::new(Expr::from(Relation::Nullary(name)));
				let fn_handle = Handle::new(Expr::from(fn_def));
				let decl = Handle::new(Expr::from(Relation::Unary(Operator::Let, fn_handle)));
				let assign = Handle::new(Expr::from(Relation::Binary(Operator::Assign, name_rel, closure_handle)));
				ret.push(decl);
				ret.push(assign);
				tree.advance_to(iter);
				Some(ret)
			},
			_ => None
		}
    }
}