use regex::Regex;
use lang::*;

static IDENT_VARNAME_TOKEN_REGEX: &'static str = "^([a-zA-Z_$][a-zA-Z_$0-9]*)";

#[derive(Clone, Debug, PartialEq)]
pub struct Identity(String);

impl Identity {
	fn new(text: String) -> Self {
		Identity(text)
	}
}

impl Into<Name> for Identity {
	fn into(self) -> Name {
		Name::new(self.0)
	}
}

impl Identity {
	pub fn find_identity_token(tree: &mut ExprTreeIterator) -> Option<Identity> {
        match tree.next() {
            Some( &Token { kind: TokenKind::Identity(ref kind @ _), .. }) => Some(kind.clone()),
            _ => None,
        }
	}
}

impl MatchToken for Identity {
	fn match_token(text: &str) -> Option<Token> {
		match_varname(text)
	}
}


fn match_varname(text: &str) -> Option<Token> {
	let name_rx = Regex::new(IDENT_VARNAME_TOKEN_REGEX).unwrap_or_else(|e| {
		panic!("Invalid regex for Identity `{:?}`. Error {:?}", IDENT_VARNAME_TOKEN_REGEX, e);
	});

	match name_rx.find(text) {
		Some((begin, end)) => {
			Some(Token::new( 
				TokenMatchPos(begin, end), 
				TokenKind::Identity(Identity::new(text[begin..end].to_owned()))
			))
		},
		None => None
	}
}