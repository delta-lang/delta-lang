use regex::Regex;
use lang::*;

static INLINE_COMMENT_TOKEN_REGEX: &'static str = r"^[//]+.*";
static MULTILINE_COMMENT_TOKEN_REGEX: &'static str = r"^/\\*(?:.|[\n\r])*?\\*/";

#[derive(Clone, Debug, PartialEq)]
pub struct Comment;

impl MatchToken for Comment {
	fn match_token(text: &str) -> Option<Token> {
		Self::match_inline(text).or_else(|| Self::match_multiline(text))
	}
}

impl Comment {
	fn match_multiline(text: &str) -> Option<Token> {
		let comment_rx = Regex::new(MULTILINE_COMMENT_TOKEN_REGEX).unwrap_or_else(|e| {
			panic!("Invalid regex for Comment `{:?}`. Error {:?}", MULTILINE_COMMENT_TOKEN_REGEX, e);
		});

		match comment_rx.find(text) {
			Some((begin, end)) => {
				Some(Token::new(
					TokenMatchPos(begin, end), 
					TokenKind::Comment(Comment)
				))
			},
			None => None
		}
	}

	fn match_inline(text: &str) -> Option<Token> {
		let comment_rx = Regex::new(INLINE_COMMENT_TOKEN_REGEX).unwrap_or_else(|e| {
			panic!("Invalid regex for Comment `{:?}`. Error {:?}", INLINE_COMMENT_TOKEN_REGEX, e);
		});

		match comment_rx.find(text) {
			Some((begin, end)) => {
				Some(Token::new(
					TokenMatchPos(begin, end), 
					TokenKind::Comment(Comment)
				))
			},
			None => None
		}
	}
}